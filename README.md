# FizzBuzz

This is a project to learn more about Docker

The project is based on javascript template from [Devscola](https://devscola.org/), you can fin it [here](https://gitlab.com/devscola/katasrunners/-/tree/master/templates/javascript)

Here we use [Jasmine](https://jasmine.github.io/)to run the test. 
Dont forget have installed install `NodeJS` and `npm`.

## First time
`npm install`

## Run test
`npm test`