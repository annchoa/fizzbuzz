const FizzBuzz = require('../src/fizzBuzz.js')

describe('FizzBuzz', () => {

    const fizz = "Fizz"
    const buzz = "Buzz"
    const fizzBuzz = fizz + buzz

    it('should return Fizz when number is multiple of 3', () => {
      const fizzBuzz = new FizzBuzz()

      const result = fizzBuzz.getString(9)

      expect(result).toBe(fizz)
    })

    it('should return Buzz when number is multiple of 5', () => {
      const fizzBuzz = new FizzBuzz()

      const result = fizzBuzz.getString(25)

      expect(result).toBe(buzz)
    })

    it('should return FizzBuzz when number is multiple of 3 and 5', () =>{
      const fizzBuzzInstance = new FizzBuzz()

      const result = fizzBuzzInstance.getString(15)

      expect(result).toBe(fizzBuzz)
    })

    it('should return string representation of the number when number is not divisible by 3 or 5', () =>{
      const fizzBuzz = new FizzBuzz()
      const number = 7
      const result = fizzBuzz.getString(number)

      expect(result).toBe(number.toString())
    })

    it('should return Fizz because has a 3 on it', () => {
      const fizzBuzz = new FizzBuzz()
      
      const result = fizzBuzz.getString(43)
      expect(result).toBe(fizz)
    })

    it('should return Buzz because has a 5 on it', () => {
      const fizzBuzz = new FizzBuzz()
      
      const result = fizzBuzz.getString(52)
      
      expect(result).toBe(buzz)
    })
})
