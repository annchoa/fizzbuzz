class FizzBuzz {
  getString(number){
    const fizz = "Fizz"
    const buzz = "Buzz"
    const fizzBuzz = fizz + buzz

    if (this.isFizzBuzz(number)){
      return fizzBuzz
    }
    if (this.isFizz(number)){
      return fizz
    }
    if (this.isBuzz(number)){
      return buzz
    }
    return number.toString()
  }

  isFizzBuzz(number){
    return this.isFizz(number) && this.isBuzz(number)
  }

  isFizz(number){
    const string_number = number.toString()
    return number % 3 === 0 || string_number.includes("3")
  }

  isBuzz(number){
    const string_number = number.toString()
    return number % 5 == 0 || string_number.includes("5")
  }

}

function init() {
  const fb = new FizzBuzz()
  document.getElementById("submit").addEventListener('click', () => {
    const value = document.getElementById("numberPlace").value
    document.getElementById("fizzBuzzResult").innerText = fb.getString(value)
  })
}

module.exports = FizzBuzz